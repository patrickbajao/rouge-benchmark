require 'bundler/setup'

Bundler.require(:default)

require_relative 'html_gitlab'

def highlight_html_formatter(filename, source)
  formatter = Rouge::Formatters::HTML.new
  lexer = Rouge::Lexer.guess(filename: filename, source: source).new
  formatter.format(lexer.lex(source))
end

def highlight_gitlab_formatter(filename, source)
  formatter = HTMLGitlab
  lexer = Rouge::Lexer.guess(filename: filename, source: source).new
  formatter.format(lexer.lex(source), tag: lexer.tag)
end
