require_relative 'highlight'
require 'benchmark'
 
json_file = File.read('index.json')
ruby_file = File.read('merge_request.rb')
 
Benchmark.bm do |benchmark|
  benchmark.report('highlight_html_formatter_json') do
    highlight_html_formatter('index.json', json_file)
  end

  benchmark.report('highlight_gitlab_formatter_json') do
    highlight_gitlab_formatter('index.json', json_file)
  end

  benchmark.report('highlight_html_formatter_ruby') do
    highlight_html_formatter('merge_request.rb', ruby_file)
  end

  benchmark.report('highlight_gitlab_formatter_ruby') do
    highlight_gitlab_formatter('merge_request.rb', ruby_file)
  end
end

Benchmark.ips do |benchmark|
  benchmark.report('highlight_html_formatter_json') do
    highlight_html_formatter('index.json', json_file)
  end

  benchmark.report('highlight_gitlab_formatter_json') do
    highlight_gitlab_formatter('index.json', json_file)
  end

  benchmark.report('highlight_html_formatter_ruby') do
    highlight_html_formatter('merge_request.rb', ruby_file)
  end

  benchmark.report('highlight_gitlab_formatter_ruby') do
    highlight_gitlab_formatter('merge_request.rb', ruby_file)
  end
end
